<?php 
    session_start();
    require_once('bdd.php');
    require_once('function.php');
    /*pour la recuperation de compte (mot de passe oublier)*/
   
    /*pour l'authentification de la connexion */
    if (isset($_POST['envoi'])) 
    {
        if ( isset($_POST['cmp'], $_POST['mdp']) AND !empty($_POST['cmp']) and !empty($_POST['mdp'])) 
        {

            $cmp=input($_POST['cmp']);
            $mdp=sha1(input($_POST['mdp']));

            $insert = auth($cmp,$mdp);
            $test=$insert->rowCount();

            if ($test==1) 
            {
                $afficher=$insert->fetch();
                $_SESSION['auth'] = array(
                    'ida' => $afficher['ida'],
                    'email' => $afficher['email'],
                    'mdp' => $afficher['mdp']
                );
                header('location:examples/dashboard.php');
            } 
            else
            {
                header('location:index.php?ida=1');
            }
        }else {
            header('location:index.php?champ=2');
        } 
    }

 ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js sidebar-large lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js sidebar-large lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js sidebar-large"> <!--<![endif]-->

<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <!-- BEGIN META SECTION -->
    <meta charset="utf-8">
    <title>Authentification</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description" />
    <meta content="themes-lab" name="author" />
    <!-- END META SECTION -->
    <!-- BEGIN MANDATORY STYLE -->
    <link href="asset_log/css/icons/icons.min.css" rel="stylesheet">
    <link href="asset_log/css/bootstrap.min.css" rel="stylesheet">
    <link href="asset_log/css/plugins.min.css" rel="stylesheet">
    <link href="asset_log/css/style.min.css" rel="stylesheet">
    <!-- END  MANDATORY STYLE -->
    <!-- BEGIN PAGE LEVEL STYLE -->
    <link href="asset_log/css/animate-custom.css" rel="stylesheet">
    <!-- END PAGE LEVEL STYLE -->
    <script src="asset_log/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>

<body class="login fade-in" data-page="login">
            <div class="col-md-4 col-sm-6 col-sm-offset-4">
                <?php 
                    if (!empty($ida) && $ida==1) 
                    { ?>
                       <div class="alert alert-warning alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong style="color: red">Attention !</strong> Verifiez votre compte ou mot de passe.
                        </div> 
              <?php } ?>
            </div>
            <div class="col-md-4 col-sm-6 col-sm-offset-4">
                <?php 
                    if (!empty($msg) && $msg==1) 
                    { ?>
                       <div class="alert alert-success alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong style="color: black">Felicitation !</strong> Vous allez recevoir un code de verification sur votre e-mail et votre numero.
                        </div> 
              <?php } ?>
            </div>
            <div class="col-md-4 col-sm-6 col-sm-offset-4">
                <?php 
                    if (!empty($cn) && $cn==1) 
                    { ?>
                       <div class="alert alert-danger alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong style="color: black">Requette en cours !</strong> Votre compte est deja Operationnelle.
                        </div> 
              <?php } ?>
            </div>
            <div class="col-md-4 col-sm-6 col-sm-offset-4">
                <?php 
                    if (!empty($champ) && $champ==2) 
                    { ?>
                       <div class="alert alert-danger alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong style="color: black">Desoler !</strong> Tous les champs doivent etre remplis.
                        </div> 
              <?php } ?>
            </div>
            <div class="col-md-4 col-sm-6 col-sm-offset-4">
                <?php 
                    if (!empty($deco) && $deco==1) 
                    { ?>
                       <div class="alert alert-success alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong style="color: black">Success !</strong> Vous etes bien deconecter.
                        </div> 
              <?php } ?>
            </div>
    <!-- BEGIN LOGIN BOX -->
    <div class="container" id="login-block">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
                <div class="login-box clearfix animated flipInY">
                    <div class="page-icon animated bounceInDown">
                        <img src="asset_log/img/account/user-icon.png" alt="Key icon">
                    </div>
                    <div class="login-logo">
                        <a href="#?login-theme-3">
                            <img src="asset_log/img/account/login-logo.png" alt="IPC-NIGER">
                        </a>
                    </div>
                    <hr>
                    <div class="login-form">
                        <form action="" method="post">
                            <input type="text" name="cmp" placeholder="Votre Compte" class="input-field form-control user" />
                            <input type="password" name="mdp" placeholder="Votre Mot de passe" class="input-field form-control password" />
                            <button type="submit" name="envoi" class="btn btn-login" style="width: 40%">Connexion</button>
                        </form>
                        <div class="login-links">
                            <a href="mdp_oublier.php">Mot de passe Oublier ?</a>
                            <br>
                            <a href="creer_cmp.php">Vous n'avez pas un Compte ? <strong>Créer</strong></a>
                        </div>
                    </div>
                </div>
                <div class="social-login row">
                    <div class="fb-login col-lg-6 col-md-12 animated flipInX">
                        <a href="#" class="btn btn-facebook btn-block">Connectez-vous <strong>Facebook</strong></a>
                    </div>
                    <div class="twit-login col-lg-6 col-md-12 animated flipInX">
                        <a href="#" class="btn btn-twitter btn-block">Connectez-vous a <strong>Twitter</strong></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END LOCKSCREEN BOX -->
    <!-- BEGIN MANDATORY SCRIPTS -->
    <script src="asset_log/plugins/jquery-1.11.js"></script>
    <script src="asset_log/plugins/jquery-migrate-1.2.1.js"></script>
    <script src="asset_log/plugins/jquery-ui/jquery-ui-1.10.4.min.js"></script>
    <script src="asset_log/plugins/bootstrap/bootstrap.min.js"></script>
    <!-- END MANDATORY SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="asset_log/plugins/backstretch/backstretch.min.js"></script>
    <script src="asset_log/js/account.js"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
</body>

</html>
